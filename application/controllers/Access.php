<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {


	public function Login()
	{
		$this->load->view('access/login');
	}
	public function Registration()
	{
		$this->load->view('access/registration');
	}
}