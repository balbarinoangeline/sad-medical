<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function dashboard(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/template/footer');
    }
    
    public function consultation(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/consultation');
        $this->load->view('admin/template/footer');
        }

    public function patient(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/patient');
        $this->load->view('admin/template/footer');
        }
    public function doctor(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/doctor');
        $this->load->view('admin/template/footer');
        }

    public function prescription(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/prescription');
        $this->load->view('admin/template/footer');
        }
     public function medicine(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/medicine');
        $this->load->view('admin/template/footer');
        }
    public function profile(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/profile');
        $this->load->view('admin/template/footer');
    }
    public function help(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/help_center');
        $this->load->view('admin/template/footer');
    }
    public function settings(){
        $this->load->view('admin/template/sidebar');
        $this->load->view('admin/template/header');
        $this->load->view('admin/settings');
        $this->load->view('admin/template/footer');
    }
}