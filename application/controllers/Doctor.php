<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

    public function dashboard(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/dashboard');
        $this->load->view('common/footer');
    }
    
    public function consultation(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/consultation');
        $this->load->view('common/footer');
        }

    public function patient(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/patient');
        $this->load->view('common/footer');
        }

    public function prescription(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/prescription');
        $this->load->view('common/footer');
        }
     public function medicine(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/medicine');
        $this->load->view('common/footer');
        }
    public function profile(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/profile');
        $this->load->view('common/footer');
    }
    public function help(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/help_center');
        $this->load->view('common/footer');
    }
    public function settings(){
        $this->load->view('common/sidebar');
        $this->load->view('common/header');
        $this->load->view('doctor/settings');
        $this->load->view('common/footer');
    }
}