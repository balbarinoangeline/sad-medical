<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

    public function consultation(){
        $this->load->view('patient/template/sidebar');
        $this->load->view('patient/template/header');
        $this->load->view('patient/consultation');
        $this->load->view('patient/template/footer');
    }
    public function prescription(){
        $this->load->view('patient/template/sidebar');
        $this->load->view('patient/template/header');
        $this->load->view('patient/prescription');
        $this->load->view('patient/template/footer');
    }
    public function profile(){
        $this->load->view('patient/template/sidebar');
        $this->load->view('patient/template/header');
        $this->load->view('patient/profile');
        $this->load->view('patient/template/footer');
    }
    public function settings(){
        $this->load->view('patient/template/sidebar');
        $this->load->view('patient/template/header');
        $this->load->view('patient/settings');
        $this->load->view('patient/template/footer');
    }
    public function help_center(){
        $this->load->view('patient/template/sidebar');
        $this->load->view('patient/template/header');
        $this->load->view('patient/help_center');
        $this->load->view('patient/template/footer');
    }
}