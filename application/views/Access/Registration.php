<DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
        <meta name="author" content="AdminKit">
        <meta name="keywords"
            content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="shortcut icon" href="img/icons/icon-48x48.png" />

        <title>Sign In | AdminKit Demo</title>
        <link href="<?= base_url();?>static/css/app.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    </head>

    <body>
        <main class="d-flex w-100">
            <div class="container d-flex flex-column">
                <div class="row vh-100">
                    <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                        <div class="d-table-cell align-middle">

                            <div class="text-center mt-4">
                                <h1 class="h2">Welcome to Medical Management System</h1>
                                <p class="lead">
                                    Create your account now
                                </p>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <div class="m-sm-4">
                                        <div class="text-center">

                                        </div>
                                        <form>
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label class="form-label">First name</label>
                                                    <input class="form-control form-control-lg" type="text" name="fname"
                                                        placeholder="Enter your first name" />
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label class="form-label">Last name</label>
                                                    <input class="form-control form-control-lg" type="text" name="lname"
                                                        placeholder="Enter your last name" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="mb-3 col-6">
                                                    <label class="form-label">Phone number</label>
                                                    <input class="form-control form-control-lg" type="text" name="num"
                                                        placeholder="Enter your number" />
                                                </div>
                                                <div class="mb-3 col-6">
                                                    <label class="form-label">Student Number</label>
                                                    <input class="form-control form-control-lg" type="text"
                                                        name="studnum" placeholder="Enter your student number" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="mb-3 col-12">
                                                    <label class="form-label">Address</label>
                                                    <input class="form-control form-control-lg" type="text" name="add"
                                                        placeholder="Enter your address" />
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="mb-3 col-12">
                                                    <label class="form-label">Email</label>
                                                    <input class="form-control form-control-lg" type="email"
                                                        name="email" placeholder="Enter your email" />
                                                </div </div>
                                                <div class="row">
                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Password</label>
                                                        <input class="form-control form-control-lg" type="password"
                                                            name="password" placeholder="Enter your password" />

                                                    </div>
                                                    <div class="mb-3 col-6">
                                                        <label class="form-label">Confirm Password</label>
                                                        <input class="form-control form-control-lg" type="password"
                                                            name="cpassword" placeholder="Enter confirm password" />

                                                    </div>
                                                </div>
                                                <div class="text-center mt-3">
                                                    <a href="<?php echo base_url() ;?>patient/consultation"
                                                        class="btn btn-lg btn-primary">Register </a>

                                                </div>
                                                <div class="text-center mt-3">
                                                    <p style="color:black;"><a href="login">Already have an account? Log
                                                            in
                                                            here!</a>
                                                    </p>

                                                </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <script src="<?php echo base_url() ;?>static/js/app.js"></script>


    </body>

    </html>

    <!-- <button type="submit" class="btn btn-lg btn-primary">Sign in</button> -->
    <style>
    .card-body {
        background: url("<?php echo base_url('src')?>/img/photos/reg.png");
        width: 900px;
        height: 550px;
        background-repeat: no-repeat;
        background-size: contain;
        margin: 60px 0px 0px 40px;
    }

    form {

        margin-left: 340px;
    }

    .btn {
        float: right;
    }
    </style>