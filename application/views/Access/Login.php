<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
    <meta name="author" content="AdminKit">
    <meta name="keywords"
        content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="img/icons/icon-48x48.png" />

    <title>Sign In | AdminKit Demo</title>
    <link href="<?= base_url();?>static/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body>
    <main class="d-flex w-100">
        <div class="container d-flex flex-column">
            <div class="row vh-100">
                <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                    <div class="d-table-cell align-middle">

                        <div class="text-center mt-4">

                            <p class="lead">

                            </p>
                        </div>

                        <div class="card">
                            <div class="card-body">

                                <div class="m-sm-1">
                                    <div class="text-center">

                                    </div>
                                    <form>
                                        <div class="mb-3">
                                            <h4 class="text-center"><b>MEDICAL MANAGEMENT SYSTEM</b></h4>
                                            <br>
                                            <label class="form-label">Email</label>
                                            <input class="form-control form-control-lg" type="email" name="email"
                                                placeholder="Enter your email" />
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Password</label>
                                            <input class="form-control form-control-lg" type="password" name="password"
                                                placeholder="Enter your password" />
                                            <small>
                                                <a href="index.html">Forgot password?</a>
                                            </small>
                                        </div>
                                        <div>
                                            <label class="form-check">
                                                <input class="form-check-input" type="checkbox" value="remember-me"
                                                    name="remember-me" checked>
                                                <span class="form-check-label">
                                                    Remember me next time
                                                </span>
                                            </label>
                                        </div>
                                        <div class="text-center mt-3">
                                            <a href="<?php echo base_url() ;?>admin/dashboard"
                                                class="btn btn-lg btn-primary">Sign in</a><br>

                                        </div>
                                        <div class="text-center mt-3">
                                            <p class="sign"><a href="access/registration">Don't have an account yet?
                                                    Sign up here!</a>
                                            </p>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="<?php echo base_url() ;?>static/js/app.js"></script>


</body>

</html>
<style>
.card-body {
    width: 730px;
    height: 350px;

}

.card {
    background: url("<?php echo base_url('src')?>/img/photos/login-bg.png");
    width: 730px;
    height: 350;
    background-repeat: no-repeat;
    background-size: contain;
}

form {

    margin-left: 340px;
}

.btn {
    width: 200px;
    float: center;
}

.sign {
    float: center;
}
</style>