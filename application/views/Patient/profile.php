
<main class="content">
	<div class="container-fluid p-0">
		<h1 class="h3 mb-3">Profile</h1>
			<div class="row">
				<div class="col-md-4 col-xl-3">
					<div class="card mb-3">
						<div class="card-header">
							<h5 class="card-title mb-0">Profile Details</h5>
						</div>
						<div class="card-body text-center">
							<img src="<?php echo base_url('src')?>/img/avatars/raf.jpg " alt="Rafael Malimban" class="img-fluid rounded-circle mb-2" width="128" height="128" />
							<h4 class="text-secondary"><strong>Rafael Malimban</strong></h4>
							<div class="text-muted mb-2 status"><h6>Active</h6>
						</div>
						<div>
						<a class="btn btn-primary btn-sm msg" href="https://www.messenger.com/login/">Messenger</a>
						<a class="btn btn-primary btn-sm viber" href="https://www.viber.com/en/"> Viber</a>
						</div>
					</div>
					<hr class="my-0" />
						<div class="card-body">
							<h4 class=" card-title">About</h4>
								<p class="text-secondary"><strong> Gender:</strong> Male</p> 
								<p class="text-secondary"><strong> Age:</strong> 21 years old</p> 
								<p class="text-secondary"><strong> Height:</strong> Unknown</p> 
								<p class="text-secondary"><strong> Weight:</strong> Unknown</p> 
								<p class="text-secondary"><strong> Weight:</strong> Blood Type</p> 
						</div>
						<hr class="my-0" />
						<div class="card-body">
							<h5 class="h6 card-title">Allergies</h5>
							<ul class="list-unstyled mb-0">
								Allergy name <br>
								Allergy name<br>
								Allergy name<br>
							</ul>
					</div>			
				</div>
			</div>
		
			<div class="col-md-8 col-xl-9">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title mb-0">Personal Information</h5>
					</div>
					<div class="card-body h-100">
						<div class="row ml-4">
							<div class="col-sm-3">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Last name</strong></h5>
								<p> Malimban</p>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Fist name</strong></h4>
								<p> Rafael</p>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Middle name</strong></h4>
								<p> Timbas</p><br>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Extension</strong></h4>
								<p> Timbas</p><br>
								</label>
							</div>
						</div>
						<div class="row ml-4">
							<div class="col-sm-3">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Birthdate: </strong></h5>
								<p> May 1, 1999</p>
								</label>
							</div>
							<div class="col-8 ml-2">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Address: </strong></h5>
								<p> Block 1 Lot 1 Barangay Bagong Silangan, Quezon Ciy</p>
								</label><br>
							</div>
						</div>
						<div class="row ml-4">
							<div class="col-sm-3">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Number: </strong></h5>
								<p> (+63) 956 0212 943</p>
								</label>
							</div>
							<div class="col-8 ml-2">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Email: </strong></h5>
								<p> rafaelmalimban@gmail.com</p><br>
								</label>
							</div>
						</div>
						<div class="row ml-4">
							<div class="col-8">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Program: </strong></h5>
								<p> Bachelor of Science in Information Technology</p>
								</label>
							</div>
						</div>
					</div>
					
				</div>
				<div class="card">
					<div class="card-header">
						<h5 class="card-title mb-0">Current Medication(s)</h5>
					</div>
					<div class="card-body">
						<div class="row">
						<div class="col-sm-3">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Medication name</strong></h5>
								<p>Advil</p>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong>Dosage</strong></h4>
								<p> 200mg</p>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> Frequncy</strong></h4>
								<p> As needed</p><br>
								</label>
							</div>
							<div class="col-sm-3 ">
								<label class="form-check">
								<h4 class="text-secondary"> <strong> How I take it:</strong></h4>
								<p> Orally</p><br>
								</label>
							</div>
							
					</div>
				</div>
			</div>
		</div>
		
	</div>
</main>
<style>
	h6{color:green;}
	.viber{background: #8A2BE2;
		border-color: #8A2BE2;}
	.viber:hover {background: #9370DB;
		border-color: #9370DB;}
	.btn-primary:not(:disabled):not(.disabled).active:focus, 
	.btn-primary:not(:disabled):not(.disabled):active:focus, 
	.show>.btn-primary.dropdown-toggle:focus {
    box-shadow: 0 0 0 0.2rem #9370db;}
	.btn-check:focus + .btn-primary, .btn-primary:focus {
		box-shadow: 0 0 0 0.2rem #dfd0ff;}
	.btn-check:focus + .btn-primary, .btn-primary:focus,  {
    background-color: #9370db;
    border-color: #9370db;
    color: #fff;}
	.msg{background: #1E90FF;
		border-color: #1E90FF;}
	.msg:hover{background: #86b1dc;
		border-color: #86b1dc}
}
</style>