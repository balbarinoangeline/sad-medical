<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">PRESCRIPTION</h4>
                    <div class="page-title-right">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <img id="logo" style="display:none" src="https://www.pup.edu.ph/about/images/PUPLogo.png"
        class="img-fluid rounded-circle" alt="">

    <img id="rxlogo" style="display:none" src="<?php echo base_url('src')?>/img/avatars/rx.png">
    <img id="signature" style="display:none" src="<?php echo base_url('src')?>/img/avatars/signature.png">




    <!-- Data Table -->

    <div class="row">
        <div class="col-20">

            <div class=" card-header">
                <h5 class="card-title mb-0">Prescription</h5>
                </h5>
            </div>

            <div class="card">
                <div class="card-body">
                    <div id="printbar" style="float:right"></div>
                    <table id="data-table"
                        class="table table-bordered table-hover table-md dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                <th class="d-none d-md-table-cell">Date</th>
                                <th class="d-none d-md-table-cell">Prescribed by</th>
                                <th class="d-none d-md-table-cell">Rx</th>
                                <th class="d-none d-md-table-cell">Sig/Remarks</th>
                                <th class="d-none d-md-table-cell">Disp</th>
                                <th class="d-none d-md-table-cell">Refills</th>
                                <th class="d-none d-md-table-cell">Action</th>
                            </tr>
                        </thead>
                        <tbody>



                            <tr>
                                <td>2021-08-01</td>
                                <td><a href="https://www.facebook.com/shane.balbarino">Angeline T. Balbarino</a></td>
                                <td>Metociopromide 10mg tab (Plasil/Metvex)</td>
                                <td>1 tab 3 x a day, before meals for 24 hours then as needed for nausea</td>
                                <td>5 pieces</td>
                                <td>0 refill</td>
                                <td>
                                    <button type="button" class="btn btn-light waves-effect"><i
                                            class="bx bx-info-circle font-size-16 align-middle" data-toggle="modal"
                                            data-target="#view"></i></button>
                                    <button type="button" class="btn btn-info waves-effect" onclick="printPass()"><i
                                            class="bx bx-printer font-size-18 align-middle"></i></button>

                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Modal for View -->
<div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="viewLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewLabel">View Prescription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="refno" class="form-label">Date</label>
                        <input type="date" name="refno" id="refno" class="form-control" value="2021-08-01" disabled>
                    </div>
                    <div class="form-group">
                        <label for="docid" class="form-label">Prescribed by</label>
                        <input type="text" name="docid" id="docid" class="form-control" value="Angeline T. Balbarino"
                            disabled>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="form-label">Rx</label>
                        <input type="text" name="reason" id="reason" class="form-control"
                            value="Metociopromide 10mg tab (Plasil/Metvex)" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Sig/Remarks</label>
                        <input type="text" name="refno" id="refno" class="form-control"
                            value="1 tab 3 x a day, before meals for 24 hours then as needed for nausea" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Disp</label>
                        <input type="text" name="refno" id="refno" class="form-control" value="5 pieces" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Refills</label>
                        <input type="text" name="refno" id="refno" class="form-control" value="0 refill" disabled>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Stay</button>-->
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url();?>static/js/patient/prescription.js"></script>