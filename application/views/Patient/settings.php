<main class="content">
	<div class="container-fluid p-0">
		<h1 class="h3 mb-3">Settings</h1>
		<div class="row">
			<div class="col-md-3 col-xl-2">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title mb-0">Profile Settings</h5>
					</div>
					<div class="list-group list-group-flush" role="tablist">
						<a class="list-group-item list-group-item-action active" data-bs-toggle="list" href="#account" role="tab">
										Account</a>
						<a class="list-group-item list-group-item-action" data-bs-toggle="list" href="#password" role="tab">
										Password</a>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-xl-10">
				<div class="tab-content">
					<div class="tab-pane fade show active" id="account" role="tabpanel">
						<div class="card">
							<div class="card-header">
								<h5 class="card-title mb-0">About</h5>
							</div>
							<div class="card-body">
								<form>
									<div class="row">
                                   		<div class="col-sm-3">
											<div class="text-center">
												<img src="<?php echo base_url('src')?>/img/avatars/raf.jpg " class="rounded-circle img-responsive mt-2"
																width="128" height="128" />
												<div class="mt-2">
													<span class="btn btn-primary"> Upload</span>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<label class="form-check">
												<h5 class="text-secondary"> <strong> Gender: </strong></h5>
												<input class="form-control form-control-sm" type="text" placeholder="Male">
											</label><br>
											<label class="form-check">
												<h5 class="text-secondary"> <strong> Weight: </strong></h5>
												<input class="form-control form-control-sm" type="text" placeholder="unknown">
											</label>
									</div>
									<div class="col-sm-3">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Age: </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="22">
										</label><br>
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Blood: </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="unknown">
										</label><br>
									</div>
									<div class="col-sm-3">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Height: </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="unknown">
										</label>
									</div>											
									</div>
									<button type="submit" class="btn btn-primary save">Save changes</button>
						</form>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
						<h5 class="card-title mb-0">Personal Information</h5>
						</div>
						<div class="card-body">
							<form> 
								<div class="row">
									<div class="col-sm-3">
										<label class="form-check">
										<h5 class="text-secondary"> <strong> Last name </strong></h5>
										<input class="form-control form-control-sm" type="text" placeholder="Malimban"><br>
										</label>
									</div>
									<div class="col-sm-3">
										<label class="form-check">
										<h5 class="text-secondary"> <strong> First name </strong></h5>
										<input class="form-control form-control-sm" type="text" placeholder="Rafael"><br>
										</label>
									</div>
									<div class="col-sm-3">
										<label class="form-check">
										<h5 class="text-secondary"> <strong> Middle name </strong></h5>
										<input class="form-control form-control-sm" type="text" placeholder="Timbas"><br>
										</label>
									</div>
									<div class="col-sm-2">
										<label class="form-check">
										<h5 class="text-secondary"> <strong> Extension </strong></h5>
										<input class="form-control form-control-sm" type="text" placeholder="none"><br>
										</label>
									</div>
									
								</div>
								<div class="row">
									<div class="col-sm-3">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Birthdate </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="May 1, 1999"><br>
										</label>
									</div>
									<div class="col-8">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Address </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="Block 1 Lot Barangay Bagong Silangan, Quezon City"><br>
										</label>
									</div>
								</div>
								<div class="row">
									<div class="col-7">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Email Address </strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="rafaelmalimban@gmail.com"><br>
										</label>
									</div>
									<div class="col-sm-4">
										<label class="form-check">
											<h5 class="text-secondary"> <strong> Number</strong></h5>
											<input class="form-control form-control-sm" type="text" placeholder="(+63) 956 0212 943"><br>
										</label>
									</div>
								</div>
								<button type="submit" class="btn btn-primary save">Save changes</button>
							</form>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="password" role="tabpanel">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Password</h5>
								<form>
									<div class="mb-3">
										<label class="form-label" for="inputPasswordCurrent">Current password</label>
										<input type="password" class="form-control" id="inputPasswordCurrent">
										<small><a href="#">Forgot your password?</a></small>
									</div>
									<div class="mb-3">
										<label class="form-label" for="inputPasswordNew">New password</label>
										<input type="password" class="form-control" id="inputPasswordNew">
									</div>
									<div class="mb-3">
										<label class="form-label" for="inputPasswordNew2">Verify password</label>
										<input type="password" class="form-control" id="inputPasswordNew2">
									</div>
									<button type="submit" class="btn btn-primary">Save changes</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<style> 
.save{float:right}
</style>