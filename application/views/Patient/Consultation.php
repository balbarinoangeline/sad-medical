<?php
if (isset($_POST['sendEmail'])) {
       $to      = 'balbarinoangeline@gmail.com';
        $subject = 'Medical Consultation Appointment';
 
         $body = 'Good day! This email confirms that Jane Doe has succesfully booked a medical consultation. The consultation session is scheduled on July 23, 2021, 2:30pm- 3:00pm with a reason for check-up. Thank you for using the Medical Management System.  This is a system-generated e-mail. Please do not reply.';
        $headers = 'From: pupmedicalmanagement@gmail.com';

        if(mail($to, $subject, $body, $headers)){
          echo "<script>toastr.success('Email successfully sent to $to')</script>"  ;
        }
        else {
           echo "<script>toastr.error('Email sending failed')</script>"  ;
        }
}
?>

<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">CONSULTATION</h4>
                    <div class="page-title-right">
                        <button type="button" class="mb-sm-4 btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">New
                            <i data-feather="plus" class="font-size-16 align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="div_form">
            <!-- Form -->
            <div class="col-md-12">
                <div class="card">
                    <!-- <div class="card-header"> -->
                    <!-- </div> -->
                    <div class="card-body">

                        <form method="POST" id="form_id" name="form_id">

                            <input type="hidden" name="uuid" id="uuid" value="" />
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="reason">Reason</label>
                                    <input type="text" class="form-control" id="reason" name="reason"
                                        placeholder="Reason" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="doctor">Doctor</label>
                                    <select id="category_name" class="form-control" data-parsley-required="true">
                                        <option selected disabled>Select Doctor</option>
                                        <option value="ange">Angeline T. Balbarino</option>
                                        <option value="ekang">Erika Velasco</option>

                                    </select>
                                </div>

                            </div>

                            <div class="row">
                                <div class="mb-3 col-md-4">
                                    <label class="form-label" for="date">Date </label>
                                    <input type="date" class="form-control" name="date" id="date">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label" for="time_from">Time From</label>
                                    <input type="time" class="form-control" id="time_from" name="time_from"
                                        data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label" for="time_to">Time To</label>
                                    <input type="time" class="form-control" id="time_to" name="time_to"
                                        data-parsley-required="true">
                                </div>


                            </div>



                            <button type="reset" class="btn btn-secondary"
                                onClick="return formReset('hide')">Cancel</button>
                            <button type="submit" class="btn btn-primary submit" id="submit"
                                name="sendEmail">Submit</button>



                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Data Table -->
        <div class="row">
            <div class="col-20">
                <div class=" card-header">
                    <h5 class="card-title mb-0">Consultation Request</h5>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table id="data-table"
                            class="table table-bordered table-hover table-sm dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Reference No</th>
                                    <th>Doctor Name</th>
                                    <th class="d-none d-xl-table-cell">Reason</th>
                                    <th class="d-none d-xl-table-cell">Date</th>
                                    <th>Time</th>
                                    <th class="d-none d-md-table-cell">Status</th>
                                    <th class="d-none d-md-table-cell">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CRN-2021720-3</td>
                                    <td><a href="https://www.facebook.com/shane.balbarino">Angeline T. Balbarino</a></td>
                                    <td>Check Up</td>
                                    <td>2021-07-20</td>
                                    <td>3:00PM - 3:30PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>
                                        <!-- View -->
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle" data-toggle="modal"
                                                data-target="#view"></i></button>
                                        
                                        <!-- Delete -->
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="cancelConsultation()"></i></button>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<script src="<?= base_url();?>static/js/patient/consultation.js"></script>

<!-- Modal for View -->
<div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="viewLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewLabel">Consultation Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="refno" class="form-label">Consultation Reference No</label>
                        <input type="text" name="refno" id="refno" class="form-control" value="TRN-2021720-3" disabled>
                    </div>
                    <div class="form-group">
                        <label for="docid" class="form-label">Doctor Name</label>
                        <input type="text" name="docid" id="docid" class="form-control" value="Angeline T. Balbarino"
                            disabled>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="form-label">Reason</label>
                        <input type="text" name="reason" id="reason" class="form-control" value="Check Up" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Date</label>
                        <input type="date" name="refno" id="refno" class="form-control" value="2021-07-20" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time Start</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="15:00" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time End</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="15:30" disabled>
                    </div>

                    <div class="form-group">
                        <label for="refno" class="form-label">Status</label>
                        <br>
                        <span class="badge badge-count badge-info">Pending</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Stay</button>-->
            </div>
        </div>
    </div>
</div>