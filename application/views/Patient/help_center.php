<main class="content">
	<div class="container-fluid p-0">
        <h1 class="h3 mb-3">Help center</h1>
           <div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title mb-0"></h5>
						</div>
					<div class="card-body bg">
                     <!-- <img src= "<?php echo base_url('src')?>/img/photos/hcBG.png" > -->
						<form class="d-none d-sm-inline-block">
                            <div class="input-group input-group-navbar search-wrapper">
                                <input type="text" class="form-control" id="search" placeholder="Search…" aria-label="Search">
                                <button class="btn" type="button">
                                <i class="align-middle" data-feather="search"></i>
                                </button>
                            </div>
                        </form>
                     </div>
				</div>
		    </div>
            <h4 class="t mb-0">Help by Category</h4><br><br>
            <div class="row pl-4">
				<div class="col-4">
					<div class="card">
						<div class="card-header">
                        <h5 class="card-title"><i class="align-middle" data-feather="info"></i> About the System</h5>
						</div>
					    <div class="card-body">
                            <a class="hover" href="">System Purpose</a><br>
                            <a class="hover" href="">User Guide</a><br>
                            <a class="hover" href="">Assitance</a><br>
                        
                        </div>
                    </div>
				</div>
                <div class="col-4">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title "><i class="align-middle" data-feather="sliders"></i>  Consultaion and Medications</h5>
						</div>
					    <div class="card-body">
                        <a class="hover" href="" >Services</a><br>
                        <a class="hover" href="" >Prescription</a><br>
                        <a class="hover" href="">Locations</a><br>
                        </div>
                    </div>
				</div>
                <div class="col-4">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title "><i class="align-middle" data-feather="user-check"></i> Reach your doctor</h5></h5>
						</div>
					    <div class="card-body ">
                            <a class="hover" href="https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin">Email Account</a><br>
                            <a class="hover" href="https://www.viber.com/en/">Viber Account</a><br>
                            <a class="hover" href="https://www.facebook.com/shane.balbarino">Facebook Account</a><br>

                        </div>
                    </div>
				</div>
		    </div>
		</div>
    </div>
</main>




 <style>
.bg{
    background: url("<?php echo base_url('src')?>/img/photos/BG.png");
    height: 500px;
    width: 1000px;
    background-repeat: no-repeat;
    background-size: contain;
 }
.search-wrapper {
    position: absolute;
    margin: 290px 0px 0px 200px;
    width: 40%;
    display: flex;
    align-items: center;
}
.hover{
    color: #222e3c;
}
a:hover {
    color: #596675;
    text-decoration: underline;
}
</style>