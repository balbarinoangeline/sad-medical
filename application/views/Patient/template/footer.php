<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-start">
                <p class="mb-0">
                    <a class="text-muted" href="https://adminkit.io/" target="_blank"><strong>Medical Management
                            System</strong></a> &copy;
                </p>
            </div>

        </div>
    </div>
</footer>
</div>
</div>

<!--  Custom JS -->

<!-- JAVASCRIPT -->
<!-- JAVASCRIPT -->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
    integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
    integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous">
</script>


<script src="<?php echo base_url() ;?>/assets/libs/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/metismenu/metisMenu.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/simplebar/simplebar.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/node-waves/waves.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/parsleyjs/parsley.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/js/pages/form-validation.init.js"></script>
<script src="<?php echo base_url() ;?>/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ;?>/assets/js/app.js"></script>
<script src="<?= base_url();?>/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="<?= base_url();?>/static/js/app.js"></script>









</body>

</html>