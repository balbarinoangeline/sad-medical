<div class="wrapper">
    <nav id="sidebar" class="sidebar js-sidebar">
        <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="dashboard">
                <span class="align-middle">Medical Management System</span>
            </a>

            <ul class="sidebar-nav">



                <li class="sidebar-item <?php echo $this->uri->segment(2) == "Consultation" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="Consultation">
                        <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Consultation</span>
                    </a>
                </li>

                <li class="sidebar-item <?php echo $this->uri->segment(2) == "Prescription" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="Prescription">
                        <i class="align-middle" data-feather="git-pull-request"></i> <span
                            class="align-middle">Prescription</span>
                    </a>
                </li>
            
            </ul>



        </div>
    </nav>