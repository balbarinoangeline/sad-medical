<div class="content">
    <div class="container-fluid p-0">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">CONSULTATION</h4>

                </div>
            </div>
        </div>




        <!-- Datatable -->
        <div class="row" id="div_form">
            <div class="col-md-12">
                <div class="card">
                    <div class=" card-header">
                        <h5 class="card-title mb-0">List of Medical Consultation</h5>
                    </div>
                    <div class="card-body">

                        <table id="data-table"
                            class="table table-bordered table-md table-hover dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Consultation Reference No</th>
                                    <th class="d-none d-xl-table-cell">Reason</th>
                                    <th>Doctor Name</th>
                                    <th>Patient Name</th>
                                    <th class="d-none d-xl-table-cell">Date</th>
                                    <th>Time</th>
                                    <th class="d-none d-md-table-cell">Status</th>
                                    <th class="d-none d-md-table-cell">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CRN-2021719-1</td>
                                    <td>Check Up</td>
                                    <td>Angeline Balbarino</td>
                                    <td>Cedrick Renigado</td>
                                    <td>2021-07-19</td>
                                    <td>1:00PM - 2:00PM</td>
                                    <td><span class="badge badge-count badge-success">Approved</span></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>CRN-2021720-2</td>
                                    <td>Check Up</td>
                                    <td>Angeline Balbarino</td>
                                    <td>Keith Spencer Habolin</td>
                                    <td>2021-07-20</td>
                                    <td>2:00PM - 2:30PM</td>
                                    <td><span class="badge badge-count badge-danger">Rejected</span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>CRN-2021720-3</td>
                                    <td>Check Up</td>
                                    <td>Angeline Balbarino</td>
                                    <td>Rafael Malimban</td>
                                    <td>2021-07-20</td>
                                    <td>3:00PM - 3:30PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>

                                        <button type="buttton" class="btn btn-light waves-effect btn-sm"
                                            class="text-success" onclick="approveConsultation()">Accept</button>
                                        <button type="buttton" class="btn btn-danger waves-effect btn-sm"
                                            class="text-danger" onclick="rejectConsultation()">Reject</button>

                                    </td>
                                </tr>
                                <tr>
                                    <td>CRN-2021721-4</td>
                                    <td>Check Up</td>
                                    <td>Angeline Balbarino</td>
                                    <td>Erika Velasco</td>
                                    <td>2021-07-21</td>
                                    <td>1:00PM - 2:00PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>
                                        <button type="buttton" class="btn btn-light waves-effect btn-sm"
                                            class="text-success">Accept</button>
                                        <button type="buttton" class="btn btn-danger waves-effect btn-sm"
                                            class="text-danger">Reject</button>
                                    </td>
                                </tr>




                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="<?= base_url();?>static/js/admin/consultation.js"></script>

<script>

</script>