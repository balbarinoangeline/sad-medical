<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">DOCTOR</h4>
                    <div class="page-title-right">
                        <button type="button" class="mb-sm-4 btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">New
                            <i data-feather="plus" class="font-size-16 align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Form -->
        <div class="row" id="div_form">
            <div class="col-md-12">
                <div class="card">
                    <!-- <div class="card-header"> -->
                    <div class="card-body">
                        <h4>Form</h4>
                        <form id="form_id" name="form_id">
                            <input type="hidden" name="uuid" id="uuid" value="" />
                            <input type="hidden" name="dentist_id" id="dentist_id" value="" />
                            <input type="hidden" name="password" id="password" value="dentist123" />
                            <input type="hidden" name="user_type" id="user_type" value="Dentist" />

                            <div class="row">
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                        placeholder="First Name" data-parsley-required="true" required>

                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Middle Name</label>
                                    <input type="text" class="form-control" id="middle_name" name="middle_name"
                                        placeholder="Middle Name">
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        placeholder="Last Name" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Extention</label>
                                    <input type="text" class="form-control" id="ext_name" name="ext_name"
                                        placeholder="Extension">
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-12">
                                    <label class="form-label" for="inputEmail4">PRC License Number</label>
                                    <input type="text" class="form-control" id="license_no" name="license_no"
                                        placeholder="PRC License Number" data-parsley-required="true" required>

                                </div>


                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputAddress">House Number</label>
                                    <input type="text" class="form-control" id="house_number" name="house_number"
                                        placeholder="Blk 44 Lot 9" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputAddress2">Street</label>
                                    <input type="text" class="form-control" id="street" name="street"
                                        placeholder="Sampaloc St" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputCity">Barangay</label>
                                    <input type="text" class="form-control" id="barangay" name="barangay"
                                        placeholder="178" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputCity">City</label>
                                    <input type="text" class="form-control" id="city" name="city" placeholder="Caloocan"
                                        data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputCity">Province</label>
                                    <input type="text" class="form-control" id="province" name="province"
                                        placeholder="Metro Manila" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label" for="inputState">Country</label>
                                    <select id="country" class="form-control" data-parsley-required="true">
                                        <option selected value="Philippines">Philippines</option>
                                        <option value="South Korea">South Korea</option>
                                        <option value="Japan">Japan</option>
                                        <option value="China">China</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">BirthDate</label>
                                    <input type="date" class="form-control" data-inputmask-alias="datetime"
                                        data-inputmask-inputformat="dd/mm/yyyy" inputmode="numeric" id="birth_date"
                                        name="birth_date" data-parsley-required="true">

                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Gender</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="gender" name="gender">
                                        <option selected>Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>

                                    </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Civil Status</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="civil_status" name="civil_status">
                                        <option selected>Civil Status</option>
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Widowed">Widowed</option>

                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Contact Number</label>
                                    <input type="text" class="form-control" inputmode="numeric" id="contact_number"
                                        placeholder="Contact Number" name="contact_number" data-parsley-required="true"
                                        data-parsley-type="number">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        placeholder="Email Address" data-parsley-required="true"
                                        data-parsley-type="email">
                                </div>


                            </div>
                            <button type="reset" class="btn btn-secondary"
                                onClick="return formReset('hide')">Cancel</button>
                            <button type="submit" class="btn btn-primary submit" id="submit"
                                name="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-20">
                <div class=" card-header">
                    <h5 class="card-title mb-0">List of Doctor</h5>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive-sm">
                            <table id="data-table"
                                class="table table-bordered  table-hover table-md dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                                style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Doctor Name</th>
                                        <th class="d-none d-xl-table-cell">License No</th>
                                        <th class="d-none d-xl-table-cell">Age</th>
                                        <th class="d-none d-xl-table-cell">Gender</th>
                                        <th class="d-none d-xl-table-cell">Contact Number</th>
                                        <th class="d-none d-xl-table-cell">Email</th>

                                        <th class="d-none d-md-table-cell">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Angeline T. Balbarino </td>
                                        <td>123456789 </td>
                                        <td>36 </td>
                                        <td>Female </td>
                                        <td>09386784102 </td>
                                        <td>balbarinoangeline@gmail.com </td>

                                        <td>
                                            <button type="button" class="btn btn-light waves-effect"><i
                                                    class="bx bx-info-circle font-size-16 align-middle"
                                                    onclick="viewData()"></i></button>
                                            <button type="button" class="btn btn-success waves-effect"><i
                                                    class="bx bx-edit font-size-16 align-middle"
                                                    onclick="editData()"></i></button>
                                            <button type="button" class="btn btn-danger waves-effect"><i
                                                    class="bx bx-trash font-size-16 align-middle"
                                                    onclick="deleteDoctor()"></i></button>


                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
function viewData() {

    toastr.success("Records retrieved successfully");
    formReset("show");
    $("#form_id input, select, textarea").prop("disabled", true);
    $("#form_id button").prop("disabled", false);
    $(".submit").hide();

    $("#first_name").val("Angeline");
    $("#middle_name").val("Tagolgol");
    $("#last_name").val("Balbarino");
    $("#license_no").val("012886328");
    $("#house_number").val("Blk 44 Lot 9");
    $("#street").val("Sampaloc St.");
    $("#barangay").val("178")
    $("#city").val("Caloocan City");
    $("#province").val("Metro Manila");
    $("#country").val("South Korea");
    $("#age").val("20");
    $("#gender").val("Female");
    $("#birth_date").val("2000-01-08");
    $("#civil_status").val("Single");
    $("#contact_number").val("09386784102");
    $("#email").val("balbarinoangeline@gmail.com");
}

function editData() {

    toastr.success("Records retrieved successfully");
    formReset("show");


    $("#first_name").val("Angeline");
    $("#middle_name").val("Tagolgol");
    $("#last_name").val("Balbarino");
    $("#license_no").val("012886328");
    $("#house_number").val("Blk 44 Lot 9");
    $("#street").val("Sampaloc St.");
    $("#barangay").val("178")
    $("#city").val("Caloocan City");
    $("#province").val("Metro Manila");
    $("#country").val("South Korea");
    $("#age").val("20");
    $("#gender").val("Female");
    $("#birth_date").val("2000-01-08");
    $("#civil_status").val("Single");
    $("#contact_number").val("09386784102");
    $("#email").val("balbarinoangeline@gmail.com");
}
</script>

<script src="<?= base_url();?>static/js/admin/doctor.js"></script>