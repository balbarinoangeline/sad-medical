<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">MEDICINE</h4>
                    <div class="page-title-right">
                        <button type="button" class="mb-sm-4 btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')"> New
                            <i data-feather="plus" class="font-size-16 align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- form -->
        <div class="row" id="div_form">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Form</h4>
                        <form id="form_id" name="form_id">
                            <input type="hidden" name="uuid" id="uuid" value="" />
                            <div class="row">
                                <div class="mb-3 col-md-8">
                                    <label class="form-label" for="inputEmail4">Medicine Name</label>
                                    <input type="text" class="form-control" id="medicine_name" name="medicine_name"
                                        placeholder="Bioflu 500g" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label" for="inputEmail4">Category </label>
                                    <select id="category_name" class="form-control" data-parsley-required="true">
                                        <option selected value="Tablets">Tablets</option>
                                        <option value="Capsules">Capsules</option>
                                        <option value="Liquid">Liquid</option>
                                        <option value="Ointment">Ointment</option>
                                        <option value="Drops">Drops</option>
                                        <option value="Inhalers">Inhalers</option>
                                        <option value="Injection">Injection</option>
                                    </select>
                                </div>


                            </div>

                            <button type="reset" class="btn btn-secondary"
                                onClick="return formReset('hide')">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
        </div>
        <!-- end form -->

        <!-- Data Table -->

        <div class="row">
            <div class="col-20">

                <div class=" card-header">
                    <h5 class="card-title mb-0">List of Medicines</h5>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div id="printbar" style="float:right"></div>
                        <table id="data-table"
                            class="table table-bordered table-hover table-md dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Medicine Name</th>
                                    <th class="d-none d-xl-table-cell">Category</th>

                                    <th class="d-none d-md-table-cell">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Biogesic</td>
                                    <td>Tablets</td>

                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(1,'view')"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(1,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deleteMedicine()"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Amoxicillin</td>

                                    <td>Capsule</td>

                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(2,'view')"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(2,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deleteMedicine()"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- </main> -->
</div>


<script src="<?= base_url();?>static/js/doctor/medicine.js"></script>
<script>
function EditOrViewData(number, editorview) {
    toastr.success("Records retrieved successfully");
    formReset("show");

    if (editorview == "view") {
        $("#form_id input, select, textarea").prop("disabled", true);
        $("#form_id button").prop("disabled", false);
        $(".submit").hide();
    }
    if (number == 1) {
        $("#medicine_name").val("Amoxicilin");
        $("#category_name").val("Capsule");

    }
    if (number == 2) {
        $("#medicine_name").val("Biogesic");
        $("#category_name").val("Tablets");

    }


}
</script>