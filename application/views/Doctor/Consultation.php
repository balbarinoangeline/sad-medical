<div class="content">
    <div class="container-fluid p-0">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">CONSULTATION</h4>

                </div>
            </div>
        </div>

        <!-- Form -->
        <div class="row" id="div_form">
            <div class="col-md-12">
                <div class="card">
                    <div class=" card-header">
                        <h5 class="card-title mb-0">List of Medical Consultation</h5>
                    </div>
                    <div class="card-body">

                        <table id="data-table"
                            class="table table-bordered table-md table-hover dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Reference No</th>
                                    <th class="d-none d-xl-table-cell">Reason</th>
                                    <th>Patient Name</th>
                                    <th class="d-none d-xl-table-cell">Date</th>
                                    <th>Time</th>
                                    <th class="d-none d-md-table-cell">Status</th>
                                    <th class="d-none d-md-table-cell">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CRN-2021719-1</td>
                                    <td>Check Up</td>
                                    <td>Cedrick Renigado</td>
                                    <td>2021-07-19</td>
                                    <td>1:00PM - 2:00PM</td>
                                    <td><span class="badge badge-count badge-success">Approved</span></td>
                                    <td>
                                        <!-- View -->
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle" data-toggle="modal"
                                                data-target="#view1"></i></button>
                                    </td>
                                </tr>

                                <tr>
                                    <td>CRN-2021720-2</td>
                                    <td>Check Up</td>
                                    <td>Keith Spencer Habolin</td>
                                    <td>2021-07-20</td>
                                    <td>2:00PM - 2:30PM</td>
                                    <td><span class="badge badge-count badge-danger">Rejected</span></td>
                                    <td>
                                        <!-- Delete -->
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle" data-toggle="modal"
                                                data-target="#view2"></i></button>
                                    </td>

                                    </td>
                                </tr>

                                <tr>
                                    <td>CRN-2021720-3</td>
                                    <td>Check Up</td>
                                    <td>Rafael Malimban</td>
                                    <td>2021-07-20</td>
                                    <td>3:00PM - 3:30PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>

                                        <button type="buttton" class="btn btn-light waves-effect btn-sm"
                                            class="text-success" onclick="approveConsultation()">Accept</button>
                                        <button type="buttton" class="btn btn-danger waves-effect btn-sm"
                                            class="text-danger" onclick="rejectConsultation()">Reject</button>

                                    </td>
                                </tr>

                                <tr>
                                    <td>CRN-2021721-4</td>
                                    <td>Check Up</td>
                                    <td>Erika Velasco</td>
                                    <td>2021-07-21</td>
                                    <td>1:00PM - 2:00PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>

                                        <button type="buttton" class="btn btn-light waves-effect btn-sm"
                                            class="text-success" onclick="approveConsultation()">Accept</button>
                                        <button type="buttton" class="btn btn-danger waves-effect btn-sm"
                                            class="text-danger" onclick="rejectConsultation()">Reject</button>

                                    </td>
                                </tr>

                                <tr>
                                    <td>CRN-2021722-5</td>
                                    <td>Check Up</td>
                                    <td>Angeline Balbarino</td>
                                    <td>2021-07-22</td>
                                    <td>4:00PM - 4:30PM</td>
                                    <td><span class="badge badge-count badge-info">Pending</span></td>
                                    <td>

                                        <button type="buttton" class="btn btn-light waves-effect btn-sm"
                                            class="text-success" onclick="approveConsultation()">Accept</button>
                                        <button type="buttton" class="btn btn-danger waves-effect btn-sm"
                                            class="text-danger" onclick="rejectConsultation()">Reject</button>

                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="<?= base_url();?>static/js/doctor/consultation.js"></script>








<!-- view 1 -->
<div class="modal fade" id="view1" tabindex="-1" role="dialog" aria-labelledby="viewLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewLabel">Consultation Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="refno" class="form-label">Reference No</label>
                        <input type="text" name="refno" id="refno" class="form-control" value="TRN-2021719-1" disabled>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="form-label">Reason</label>
                        <input type="text" name="reason" id="reason" class="form-control" value="Check Up" disabled>
                    </div>
                    <div class="form-group">
                        <label for="docid" class="form-label">Patient Name</label>
                        <input type="text" name="docid" id="docid" class="form-control" value="Cedrick Renigado"
                            disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Date</label>
                        <input type="date" name="refno" id="refno" class="form-control" value="2021-07-19" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time Start</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="13:00" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time End</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="14:00" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Status</label>
                        <br>
                        <span class="badge badge-count badge-success">Approved</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Stay</button>-->
            </div>
        </div>
    </div>
</div>

<!-- view 2 -->
<div class="modal fade" id="view2" tabindex="-1" role="dialog" aria-labelledby="viewLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewLabel">Consultation Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="refno" class="form-label">Reference No</label>
                        <input type="text" name="refno" id="refno" class="form-control" value="CRN-2021720-2" disabled>
                    </div>
                    <div class="form-group">
                        <label for="reason" class="form-label">Reason</label>
                        <input type="text" name="reason" id="reason" class="form-control" value="Check Up" disabled>
                    </div>
                    <div class="form-group">
                        <label for="docid" class="form-label">Patient Name</label>
                        <input type="text" name="docid" id="docid" class="form-control" value="Keith Spencer Habolin"
                            disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Date</label>
                        <input type="date" name="refno" id="refno" class="form-control" value="2021-07-20" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time Start</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="14:00" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Time End</label>
                        <input type="time" name="refno" id="refno" class="form-control" value="14:30" disabled>
                    </div>
                    <div class="form-group">
                        <label for="refno" class="form-label">Status</label>
                        <br>
                        <span class="badge badge-count badge-danger">Rejected</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Stay</button>-->
            </div>
        </div>
    </div>
</div>