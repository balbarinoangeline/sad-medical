<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">PATIENT</h4>
                    <div class="page-title-right">
                        <button type="button" class="mb-sm-4 btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">New
                            <i data-feather="plus" class="font-size-16 align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="div_form">
            <!-- Form -->
            <div class="col-md-12">
                <div class="card">
                    <!-- <div class="card-header"> -->
                    <!-- </div> -->
                    <div class="card-body">
                        <h4>Form</h4>
                        <form id="form_id" name="form_id">

                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="font-weight-bold">PART I. STUDENT INFORMATION</label>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                        placeholder="First Name" data-parsley-required="true" required>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Middle Name</label>
                                    <input type="text" class="form-control" id="middle_name" name="middle_name"
                                        placeholder="Middle Name">
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        placeholder="Last Name" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label class="form-label" for="inputEmail4">Extention</label>
                                    <input type="text" class="form-control" id="ext_name" name="ext_name"
                                        placeholder="Extension">
                                </div>
                            </div>


                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputAddress">Home Address </label>
                                    <input type="text" class="form-control" id="home_address" name="home_address"
                                        placeholder="House Number" data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label">Age</label>
                                    <input type=text class="form-control" inputmode="numeric" id="age" name="age"
                                        data-parsley-required="true">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label">Gender</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="gender" name="gender">
                                        <option selected>Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>

                                </div>
                                <div class="mb-3 col-md-2">
                                    <label class="form-label">Civil Status</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="civil_status" name="civil_status">
                                        <option selected>Civil Status</option>
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Widowed">Widowed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">

                                <div class="mb-3 col-md-8">
                                    <label class="form-label">Program</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="program" name="program">
                                        <option selected>Bachelor in Business Teacher Education </option>
                                        <option value="BSBA">Bachelor of Business Administration</option>
                                        <option value="BPA">Bachelor of Public Administration major in Public Financial
                                            Management</option>
                                        <option value="BSBAHR">Bachelor of Science in Business Administration major in
                                            Human
                                            Resource Development Management</option>
                                        <option value="BSBAMM">Bachelor of Science in Business Administration major in
                                            Marketing Management</option>
                                        <option value="BSENT">Bachelor of Science in Entrepreneurial Management
                                        </option>
                                        <option value="BSIT">Bachelor of Science in Information Technology</option>
                                        <option value="DOMT">Diploma in Office Management Technology</option>
                                    </select>
                                </div>

                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Blood Type</label>
                                    <select class="form-select" aria-label="Default select example"
                                        data-parsley-required="true" id="blood_type" name="blood_type">
                                        <option value="A">A</option>

                                        <option value="A+">A</option>
                                        <option value="B">B</option>
                                        <option value="AB">AB</option>
                                        <option value="O">O</option>
                                        <option value="O-">O-</option>



                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Contact Number</label>
                                    <input type="text" class="form-control" inputmode="numeric" id="contact_number"
                                        placeholder="Contact Number" name="contact_number" data-parsley-required="true"
                                        data-parsley-type="number">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label class="form-label">Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                        placeholder="Email Address" data-parsley-required="true"
                                        data-parsley-type="email">
                                </div>
                                <input type="hidden" name="user_type" id="user_type" value="Patient" />
                                <div class="mb-3 col-md-4" id="hidePass">
                                    <label class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" name="password"
                                        placeholder="Password">
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="font-weight-bold">PART II. MEDICAL HISTORY</label>
                                </div>
                                <div class="mb-2 col-md-12">

                                    <label class="font-weight-bold">1. Do you need medical
                                        attention or has known medical
                                        illness?</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="mhno">
                                        <label class="form-check-label" for="inlineCheckbox1">
                                            No</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="mhyes">
                                        <label class="form-check-label" for="inlineCheckbox2">Yes</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="font-weight-italic">(Please check the following that apply and give
                                        more information as needed)</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="asthma">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Asthma
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="diabetes">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Diabetes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="seizure">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Seizure Disorder
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="migraine">
                                        <label class="form-check-label" for="flexCheckChecked">
                                            Migraine
                                        </label>
                                    </div>
                                </div>


                                <div class="mb-3 col-md-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="fainting">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Fainting
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="heart">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Heart Condition
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="hyperventilation">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Hyperventilation
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="hypertenion">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Hypertension
                                        </label>
                                    </div>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="eye">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Eye Disease/Defect
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="kidney">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Kidney Disease
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="hemophilia">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Hemophilia
                                        </label>
                                    </div>
                                </div>

                            </div>


                            <!-- <div class="row">
                                 <div class="mb-2 col-md-12">
                                     <label class="font-weight-bold">1. Previous Hospitalization</label>
                                     <div class="form-check form-check-inline">
                                         <input class="form-check-input" type="radio" id="inlineCheckbox1"
                                             value="option1">
                                         <label class="form-check-label" for="inlineCheckbox1">
                                             No</label>
                                     </div>
                                     <div class="form-check form-check-inline">
                                         <input class="form-check-input" type="radio" id="inlineCheckbox2"
                                             value="option2">
                                         <label class="form-check-label" for="inlineCheckbox2">Yes</label>
                                     </div>
                                     <div class="row">
                                         <div class="mb-2 col-md-12"> <label class="font-weight-bold">2.
                                                 Operation/Surgery</label>
                                             <div class="form-check form-check-inline">
                                                 <input class="form-check-input" type="radio" id="inlineCheckbox1"
                                                     value="option1">
                                                 <label class="form-check-label" for="inlineCheckbox1">
                                                     No</label>
                                             </div>
                                             <div class="form-check form-check-inline">
                                                 <input class="form-check-input" type="radio" id="inlineCheckbox2"
                                                     value="option2">
                                                 <label class="form-check-label" for="inlineCheckbox2">Yes</label>
                                             </div>
                                         </div>

                                     </div>

                                 </div>
                             </div> -->

                            <div class="row">
                                <div class="mb-3 col-md-12">
                                    <label class="font-weight-bold">2. Additional Information for Students with Medical
                                        Conditions:</label>
                                    <input type="text" class="form-control" inputmode="numeric" id="allergies"
                                        placeholder="Allergies" data-parsley-required="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="font-weight-bold">PART III. FAMILY HISTORY</label>
                                </div>
                                <div class="mb-2 col-md-12">


                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="fhdiabetes">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Diabetes
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="autoimmune">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Auto immune Disease
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="fhhypertension">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Hypertension
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="cancer">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Cancer
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="font-weight-bold">PART IV. PERSONAL HISTORY</label>
                                </div>
                                <div class="mb-2 col-md-12">

                                    <label class="form-label">Cigarette Smoking:</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="csno">
                                        <label class="form-check-label" for="inlineCheckbox1">
                                            No</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="csyes">
                                        <label class="form-check-label" for="inlineCheckbox2">Yes</label>
                                    </div>
                                </div>
                                <div class="mb-2 col-md-12">
                                    <label class="form-label">Alcohol Drinking:</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="adno">
                                        <label class="form-check-label" for="inlineCheckbox1">
                                            No</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="adyes">
                                        <label class="form-check-label" for="inlineCheckbox2">Yes</label>
                                    </div>
                                </div>
                            </div>



                            <button type="reset" class="btn btn-secondary"
                                onClick="return formReset('hide')">Cancel</button>
                            <button type="submit" class="btn btn-primary submit" id="submit"
                                name="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Data Table -->
        <div class="row">
            <div class="col-20">
                <div class=" card-header">
                    <h5 class="card-title mb-0">List of Patient</h5>
                </div>
                <div class="card">
                    <div class="card-body">
                        <table id="data-table"
                            class="table table-bordered table-hover table-md dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="d-none d-md-table-cell">Patient Name</th>
                                    <th class="d-none d-md-table-cell">Age</th>
                                    <th class="d-none d-md-table-cell">Gender</th>
                                    <th class="d-none d-md-table-cell">Blood Type</th>

                                    <th class="d-none d-md-table-cell">Status</th>
                                    <th class="d-none d-md-table-cell">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Cedrick Renigado</td>
                                    <td>17</td>
                                    <td>Male</td>
                                    <td>O-</td>
                                    <td><span class="badge badge-count badge-success">Active</span></td>
                                    <td>
                                        <!-- View Button -->
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(1,'view')"></i></button>

                                        <!-- Edit Button -->
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(1,'edit')"></i></button>

                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePatient()"></i></button>
                                    </td>

                                <tr>
                                    <td>Keith Spencer Habolin</td>
                                    <td>24</td>
                                    <td>Male</td>
                                    <td>A</td>
                                    <td><span class="badge badge-count badge-success">Active</span></td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(2,'view')"></i></button>

                                        <!-- Edit Button -->
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(2,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePatient()"></i></button>
                                    </td>
                                </tr>



                                <tr>
                                    <td>Erika Velasco</td>
                                    <td>25</td>
                                    <td>Female</td>
                                    <td>A+</td>
                                    <td><span class="badge badge-count badge-success">Active</span></td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(3,'view')"></i></button>

                                        <!-- Edit Button -->
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(3,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePatient()"></i></button>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Rafael Malimban</td>
                                    <td>29</td>
                                    <td>Male</td>
                                    <td>A</td>
                                    <td><span class="badge badge-count badge-success">Active</span></td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"><i
                                                class="bx bx-info-circle font-size-16 align-middle"
                                                onclick="EditOrViewData(4,'view')"></i></button>

                                        <!-- Edit Button -->
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(4,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePatient()"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<script src="<?= base_url();?>static/js/doctor/patient.js"></script>