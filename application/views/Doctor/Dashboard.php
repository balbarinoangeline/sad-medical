<main class="content">
    <div class="container-fluid p-0">

        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3> Dashboard</h3>
            </div>


        </div>
        <div class="row">
            <div class="col-xl-12 d-flex">
                <div class="w-100">
                    <div class="row">
                        <div class="col-md-3">

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col mt-0">
                                            <h5 class="card-title">Consultation Session</h5>
                                        </div>

                                        <div class="col-auto">
                                            <div class="stat text-primary">
                                                <i class="align-middle" data-feather="briefcase"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="mt-1 mb-3">382</h1>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col mt-0">
                                            <h5 class="card-title">Patients</h5>
                                        </div>

                                        <div class="col-auto">
                                            <div class="stat text-primary">
                                                <i class="align-middle" data-feather="users"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="mt-1 mb-3">1,256</h1>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col mt-0">
                                            <h5 class="card-title">Prescription</h5>
                                        </div>

                                        <div class="col-auto">
                                            <div class="stat text-primary">
                                                <i class="align-middle" data-feather="git-pull-request"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="mt-1 mb-3">64</h1>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col mt-0">
                                            <h5 class="card-title">Medicines</h5>
                                        </div>

                                        <div class="col-auto">
                                            <div class="stat text-primary">
                                                <i class="align-middle" data-feather="briefcase"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <h1 class="mt-1 mb-3">64</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-xxl-3 d-flex order-1 order-xxl-3">
                <div class="card flex-fill w-100">
                    <div class="card-header">
                        <div class="card-actions float-end">
                            <div class="dropdown show">
                                <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                    <i class="align-middle" data-feather="more-horizontal"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title mb-0">Programs</h5>
                    </div>
                    <div class="card-body d-flex">
                        <div class="align-self-center w-100">
                            <div class="py-3">
                                <div class="chart chart-xs">
                                    <canvas id="chartjs-dashboard-pie"></canvas>
                                </div>
                            </div>

                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <td><i class="fas fa-circle text-primary fa-fw"></i> BSIT <span
                                                class="badge badge-success-light">+12%</span></td>
                                        <td class="text-end">4306</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fas fa-circle text-warning fa-fw"></i> BSBA - MM <span
                                                class="badge badge-danger-light">-3%</span></td>
                                        <td class="text-end">3801</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fas fa-circle text-danger fa-fw"></i> BSENT</td>
                                        <td class="text-end">1689</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fas fa-circle text-dark fa-fw"></i> DOMT</td>
                                        <td class="text-end">3251</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-12 col-xxl-6 d-flex order-3 order-xxl-2">
                <div class="card flex-fill w-100">
                    <div class="card-header">
                        <div class="float-end">
                            <form class="row g-2">
                                <div class="col-auto">
                                    <select class="form-select form-select-sm bg-light border-0">
                                        <option>Jan</option>
                                        <option value="1">Feb</option>
                                        <option value="2">Mar</option>
                                        <option value="3">Apr</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <input type="text" class="form-control form-control-sm bg-light rounded-2 border-0"
                                        style="width: 100px;" placeholder="Search..">
                                </div>
                            </form>
                        </div>
                        <h5 class="card-title mb-0">Consultation Meetings</h5>
                    </div>
                    <div class="card-body pt-2 pb-3">
                        <div class="chart chart-sm">
                            <canvas id="chartjs-dashboard-line"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-1">
                <div class="card flex-fill">
                    <div class="card-header">
                        <div class="card-actions float-end">
                            <div class="dropdown show">
                                <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                    <i class="align-middle" data-feather="more-horizontal"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <h5 class="card-title mb-0">Calendar</h5>
                    </div>
                    <div class="card-body d-flex">
                        <div class="align-self-center w-100">
                            <div class="chart">
                                <div id="datetimepicker-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">

            <div class="row">
                <div class="col-xl-12 d-flex">
                    <div class="w-100">
                        <div class="card flex-fill">
                            <div class="card-header">
                                <div class="card-actions float-end">
                                    <div class="dropdown show">
                                        <a href="#" data-bs-toggle="dropdown" data-bs-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">Today's Consultation Appointment</h5>
                            </div>
                            <table class="table table-borderless  ml-2">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th class="d-none d-xxl-table-cell">Date</th>
                                        <th class="d-none d-xl-table-cell">Time</th>
                                        <th>Status</th>
                                        <th class="d-none d-xl-table-cell">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Angeline Balbarino</td>
                                        <td>2021-07-19</td>
                                        <td>4:00PM - 4:30PM</td>
                                        <td><span class="badge badge-count badge-info">Pending</span></td>
                                        <td class="d-none d-xl-table-cell">
                                            <a href="#" class="btn btn-light">View</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Erika Velasco</td>
                                        <td>2021-07-19</td>
                                        <td>1:00PM - 2:00PM</td>
                                        <td><span class="badge badge-count badge-info">Pending</span></td>
                                        <td class="d-none d-xl-table-cell">
                                            <a href="#" class="btn btn-light">View</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Rafael Malimban</td>
                                        <td>2021-07-19</td>
                                        <td>3:00PM - 3:30PM</td>
                                        <td><span class="badge badge-count badge-info">Pending</span></td>
                                        <td class="d-none d-xl-table-cell">
                                            <a href="#" class="btn btn-light">View</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Cedrick Renigado</td>
                                        <td>2021-07-19</td>
                                        <td>1:00PM - 2:00PM</td>
                                        <td><span class="badge badge-count badge-success">Approved</span></td>
                                        <td class="d-none d-xl-table-cell">
                                            <a href="#" class="btn btn-light">View</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Keith Spencer Habolin</td>
                                        <td>2021-07-19</td>
                                        <td>2:00PM - 2:30PM</td>
                                        <td><span class="badge badge-count badge-danger">Rejected</span></td>
                                        <td class="d-none d-xl-table-cell">
                                            <a href="#" class="btn btn-light">View</a>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>



    </div>
</main>

<script src="<?= base_url();?>static/js/doctor/dashboard.js"></script>