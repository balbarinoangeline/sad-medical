<div class="content">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-4 font-size-18 font-weight-bold">PRESCRIPTION</h4>
                    <div class="page-title-right">
                        <button type="button" class="mb-sm-4 btn btn-success waves-effect waves-light" id="btn_add"
                            onClick="return formReset('show')">New
                            <i data-feather="plus" class="font-size-16 align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- form -->
        <div class="row" id="div_form">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Form</h4>
                        <form id="form_id" name="form_id">

                            <div class="row">
                                <div class="mb-3 col-md-12">
                                    <label class="form-label">Student Name</label>
                                    <select class="form-control select2" id="student" name="student"
                                        data-parsley-required="true">
                                        <option value="rafael" selected>Rafael Malimban</option>
                                        <option value="keith">Keith Spencer Habolin</option>
                                        <option value="erika">Erika Velasco</option>
                                        <option value="cedrick">Cedrick Renegado</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputEmail4">Rx</label>
                                    <textarea type="text" class="form-control" id="rx" name="rx" placeholder=""
                                        data-parsley-required="true" required></textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputEmail4">Sig/Remarks</label>
                                    <textarea type="text" class="form-control" id="sig" name="sig" placeholder=""
                                        data-parsley-required="true" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputEmail4">Disp</label>
                                    <textarea type="text" class="form-control" id="disp" name="disp"
                                        data-parsley-required="true" required></textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputEmail4">Refills</label>
                                    <textarea type="text" class="form-control" id="refill" name="refill" placeholder=""
                                        data-parsley-required="true" required></textarea>

                                </div>
                            </div>


                            <button type="reset" class="btn btn-secondary"
                                onClick="return formReset('hide')">Cancel</button>
                            <button type="submit" class="btn btn-primary submit" id="submit"
                                name="submit">Submit</button>
                        </form>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
        </div>
        <!-- end form -->

        <!-- Data Table -->

        <div class="row">
            <div class="col-20">

                <div class=" card-header">
                    <h5 class="card-title mb-0">List of Prescription</h5>
                    </h5>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div id="printbar" style="float:right"></div>
                        <table id="data-table"
                            class="table table-bordered table-hover table-md dt-responsive wrap w-100 dataTable no-footer dtr-inline"
                            style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="d-none d-md-table-cell">Date</th>
                                    <th class="d-none d-md-table-cell">Patient Name</th>
                                    <th class="d-none d-md-table-cell">Rx</th>
                                    <th class="d-none d-md-table-cell">Sig/Remarks</th>
                                    <th class="d-none d-md-table-cell">Disp</th>
                                    <th class="d-none d-md-table-cell">Refills</th>
                                    <th class="d-none d-md-table-cell">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>2021-08-1</td>
                                    <td>Keith Spencer Habolin</td>
                                    <td>Bioflu 500g</td>
                                    <td>Take this medicines for 3 days</td>
                                    <td>9 pieces</td>
                                    <td>1 refill</td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"
                                            onclick="EditOrViewData(1,'view')"><i
                                                class="bx bx-info-circle font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"
                                            onclick="EditOrViewData(1,'edit')"><i
                                                class="bx bx-edit font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePrescription()"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2021-04-22</td>
                                    <td>Rafael Malimban</td>
                                    <td>Aspririn 250 mg</td>
                                    <td>Drink this every 6 hours</td>
                                    <td>5 pieces</td>
                                    <td>2 refill</td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"
                                            onclick="EditOrViewData(2,'view')"><i
                                                class="bx bx-info-circle font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"
                                            onclick="EditOrViewData(2,'edit')"><i
                                                class="bx bx-edit font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePrescription()"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2021-09-12</td>
                                    <td>Cedrick Renegado</td>
                                    <td>Medicol 500g</td>
                                    <td>Take 1 tablet by mouth every six hours for headaches</td>
                                    <td>2 tablets</td>
                                    <td>3 refill</td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"
                                            onclick="EditOrViewData(3,'view')"><i
                                                class="bx bx-info-circle font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"
                                            onclick="EditOrViewData(3,'edit')"><i
                                                class="bx bx-edit font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePrescription()"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2021-03-17</td>
                                    <td>Erika Velasco</td>
                                    <td>Livostin </td>
                                    <td>one drop of Livostin four times per day in each eye for 7 days.</td>
                                    <td>24 tablets</td>
                                    <td>1 refill</td>
                                    <td>
                                        <button type="button" class="btn btn-light waves-effect"
                                            onclick="EditOrViewData(4,'view')"><i
                                                class="bx bx-info-circle font-size-16 align-middle"></i></button>
                                        <button type="button" class="btn btn-success waves-effect"><i
                                                class="bx bx-edit font-size-16 align-middle"
                                                onclick="EditOrViewData(4,'edit')"></i></button>
                                        <button type="button" class="btn btn-danger waves-effect"><i
                                                class="bx bx-trash font-size-16 align-middle"
                                                onclick="deletePrescription()"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<script src="<?= base_url();?>static/js/doctor/prescription.js"></script>