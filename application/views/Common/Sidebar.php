<div class="wrapper">
    <nav id="sidebar" class="sidebar js-sidebar">
        <div class="sidebar-content js-simplebar">
            <a class="sidebar-brand" href="dashboard">
                <span class="align-middle">Medical Management System</span>
            </a>

            <ul class="sidebar-nav">



                <li class="sidebar-item <?php echo $this->uri->segment(2) == "dashboard" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="dashboard">
                        <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item <?php echo $this->uri->segment(2) == "consultation" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="consultation">
                        <i class="align-middle" data-feather="briefcase"></i> <span
                            class="align-middle">Consultation</span>
                    </a>
                </li>
                <li class="sidebar-item <?php echo $this->uri->segment(2) == "patient" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="patient">
                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">Patient</span>
                    </a>
                </li>
                <li class="sidebar-item <?php echo $this->uri->segment(2) == "prescription" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="prescription">
                        <i class="align-middle" data-feather="git-pull-request"></i> <span
                            class="align-middle">Prescription</span>
                    </a>
                </li>
                <li class="sidebar-item <?php echo $this->uri->segment(2) == "medicine" ?  "active" :  "" ?>">
                    <a class="sidebar-link" href="medicine">
                        <i class="align-middle" data-feather="book"></i> <span class="align-middle">Medicine</span>
                    </a>
                </li>

            </ul>



        </div>
    </nav>