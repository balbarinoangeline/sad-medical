$(document).ready(function () {
	$("#data-table").DataTable();
});
function printPass() {
	var imgSrc = document.getElementById("logo").src;
	var signature = document.getElementById("signature").src;

	var date = new Date();
	var dateStr =
		("00" + (date.getMonth() + 1)).slice(-2) +
		"/" +
		("00" + date.getDate()).slice(-2) +
		"/" +
		date.getFullYear() +
		" " +
		("00" + date.getHours()).slice(-2) +
		":" +
		("00" + date.getMinutes()).slice(-2) +
		":" +
		("00" + date.getSeconds()).slice(-2);
	console.log(dateStr);
	var image = document.getElementById("rxlogo").src;
	var last_name = "Malimban";
	var first_name = "Rafael";
	var middle_name = "Timbas";
	var age = "21";
	var gender = "Male";
	var Doctorname = "Angeline Balbarino";
	var rx = "Metociopromide 10mg tab (Plasil/Metvex)";
	var sig =
		"1 tab 3 x a day, before meals for 24 hours then as needed for nausea";
	var disp = "5 pieces";
	var refill = "0";

	var win = window.open("", "_new");
	win.document.open();
	win.document.write(
		[
			"<html>",
			"   <head>",
			'       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">',

			"   </head>",
			'   <body onload="window.print()" onafterprint="window.close()">',
			'<img style="height:70px; width:70px" src="' +
				imgSrc +
				'"/><label style="margin-left: 130px;font-weight: bold;font-size: 30px">POLYTECHNIC UNIVERSITY OF THE PHILIPPINES</label><div class="vl"></div>',
			// '<h5 style="text-align:center;font-weight:bold">Polytechnic University of the Philippines</h5>',
			'<h5 style="text-align:center">Quezon City Branch</h5>',
			'<h5 style="text-align:center">Don Fabian, Quezon City, 1121 Metro Manila</h5>',
			"<br>",
			'<p style="font-size: 22px; text-align: right">LIC #93123' +
				"    " +
				"PTR #93123",

			'<p style="font-size: 25px">Date:<b>' + " " + dateStr + "</b></p>",
			'<p style="font-size: 25px" >Name:<b>' +
				" " +
				last_name +
				", " +
				first_name +
				" " +
				middle_name +
				"</b></p>",
			'<p style="font-size: 25px" >Age:<b>' + " " + age + "</b></p>",
			'<p style="font-size: 25px">Gender:<b>' + " " + gender + "</b></p>",
			"<hr>",
			"<br><br>",
			'<img style="height:70px; width:70px" src="' +
				image +
				'"/><div class="vl"></div>',

			'<p style="font-size: 25px;margin-left: 50px;"><b>Rx:</b>' +
				" " +
				rx +
				"</p>",
			'<p style="font-size: 25px;margin-left: 50px;"><b>Sig/Remarks:</b>' +
				" " +
				sig +
				"</p>",
			'<p style="font-size: 25px;margin-left: 50px;"><b>Disp:</b>' +
				" " +
				disp +
				"</p>",
			'<p style="font-size: 25px;margin-left: 50px; "><b>Refills:</b>' +
				" " +
				refill +
				"</p>",
			"<br><br>",
			'<img style="height:70px; width:70px; float:right; margin-right: 70px;" src="' +
				signature +
				'"/>',
			"<br><br><br><br>",
			'<p style="text-align: right;font-size: 25px"><b>' +
				Doctorname +
				", MD" +
				"</b></p>",
			'<p style="text-align: right;font-size: 20px; margin-right: 50px;">Internal Medicine',
			"<br>",
			'<p style="text-align: right;font-size: 20px;  margin-right: 50px;">Medical Officer III' +
				"   </body>",
			"</html>",
		].join("")
	);
	win.document.close();
}
