$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});

$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to reject consultation
function cancelConsultation() {
	Swal.fire({
		title: "Are you sure you want to cancel this appointment",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, cancel it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success(" Cancelled Consultation Appointment Successfully");
		}
	});
}
