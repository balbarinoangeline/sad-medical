$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});

$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to delete patient
function deletePatient() {
	Swal.fire({
		title: "Are you sure you want to delete this record",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully deleted a patient");
		}
	});
}

function update() {
	toastr.success("Updated Successfully");
}

// function to show details for viewing/updating
function EditOrViewData(number, editorview) {
	console.log(editorview);
	toastr.success("Records retrieved successfully");
	formReset("show");

	if (editorview == "view") {
		$("#form_id input, select, textarea").prop("disabled", true);
		$("#form_id button").prop("disabled", false);
		$(".submit").hide();
	}
	if (number == 1) {
		$("#first_name").val("Cedrick");
		$("#middle_name").val("Reyes");
		$("#last_name").val("Renegado");
		$("#home_address").val("Commonwealth Quezon City");
		$("#age").val("17");
		$("#gender").val("Male");
		$("#civil_status").val("Single");
		$("#program").val("BSIT");
		$("#blood_type").val("O-");
		$("#contact_number").val("09123456789");
		$("#email").val("cedrickerenegado@gmail.com");
	}
	if (number == 2) {
		$("#first_name").val("Erika");
		$("#middle_name").val("");
		$("#last_name").val("Velasco");
		$("#home_address").val("Commonwealth Quezon City");
		$("#age").val("25");
		$("#gender").val("Female");
		$("#civil_status").val("Married");
		$("#program").val("BSIT");
		$("#blood_type").val("A+");
		$("#contact_number").val("09123456789");
		$("#email").val("erikavelasco@gmail.com");
	}
	if (number == 3) {
		$("#first_name").val("Keith Spencer");
		$("#middle_name").val("Reys");
		$("#last_name").val("Habolin");
		$("#home_address").val("Commonwealth Quezon City");
		$("#age").val("24");
		$("#gender").val("Male");
		$("#civil_status").val("Married");
		$("#program").val("BSIT");
		$("#blood_type").val("A");
		$("#contact_number").val("09123456789");
		$("#email").val("Habolin@gmail.com");
	}
	if (number == 4) {
		$("#first_name").val("Rafael");
		$("#middle_name").val("");
		$("#last_name").val("Malimban");
		$("#home_address").val("Commonwealth Quezon City");
		$("#age").val("29");
		$("#gender").val("Male");
		$("#civil_status").val("Single");
		$("#program").val("BSIT");
		$("#blood_type").val("A");
		$("#contact_number").val("09123456789");
		$("#email").val("rafaelmalimban@gmail.com");
	}
	$("#mhno").attr("checked", true);
	$("#mhyes").attr("checked", false);
	$("#asthma").attr("checked", true);
	$("#diabetes").attr("checked", false);
	$("#seizure").attr("checked", true);
	$("#migraine").attr("checked", false);
	$("#fainting").attr("checked", true);
	$("#heart").attr("checked", false);
	$("#hyperventilation").attr("checked", true);
	$("#hypertension").attr("checked", false);
	$("#eye").attr("checked", true);
	$("#kidney").attr("checked", false);
	$("#hemophilia").attr("checked", false);
	$("#allergies").val("Seafood");
	$("#fhdiabetes").attr("checked", false);
	$("#autoimmune").attr("checked", false);
	$("#fhhypertension").attr("checked", true);
	$("#cancer").attr("checked", false);
	$("#csno").attr("checked", true);
	$("#csyes").attr("checked", false);
	$("#adno").attr("checked", true);
	$("#adyes").attr("checked", false);
}
