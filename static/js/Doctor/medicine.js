$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});
$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to delete  medicine
function deleteMedicine() {
	Swal.fire({
		title: "Are you sure you want to delete this record",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully deleted a medicine");
		}
	});
}

function update() {
	toastr.success("Updated Successfully");
}

// function to show details for viewing/updating
function EditOrViewData(number, editorview) {
	console.log(editorview);
	toastr.success("Records retrieved successfully");
	formReset("show");

	if (editorview == "view") {
		$("#form_id input, select, textarea").prop("disabled", true);
		$("#form_id button").prop("disabled", false);
		$(".submit").hide();
	}
	if (number == 1) {
		$("#medicine_name").val("Biogesic 500g");
		$("#category_name").val("Tablets");
	}
	if (number == 2) {
		$("#medicine_name").val("Amoxicillin 250g");
		$("#catgory_name").val("Capsule");
	}
}
