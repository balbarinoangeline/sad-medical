$(function () {
	// formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});
$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to approve consultation
function approveConsultation() {
	Swal.fire({
		title: "Are you sure you want to approve this appointment",
		text: "You won't be able to revert this!",
		icon: "success",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#8f8483",
		confirmButtonText: "Yes, approve it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully Approved a Consultation Appointment");
		}
	});
}

// function to reject consultation
function rejectConsultation() {
	Swal.fire({
		title: "Are you sure you want to reject this appointment",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, reject it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully Rejected a Consultation Appointment");
		}
	});
}
