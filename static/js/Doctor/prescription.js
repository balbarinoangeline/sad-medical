$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});

$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to delete patient
function deletePrescription() {
	Swal.fire({
		title: "Are you sure you want to delete this record",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully deleted a prescription");
		}
	});
}

function update() {
	toastr.success("Updated Successfully");
}

// function to show details for viewing/updating
function EditOrViewData(number, editorview) {
	console.log(editorview);
	toastr.success("Records retrieved successfully");
	formReset("show");

	if (editorview == "view") {
		$("#form_id input, select, textarea").prop("disabled", true);
		$("#form_id button").prop("disabled", false);
		$(".submit").hide();
	}
	if (number == 1) {
		$("#student").val("keith");
		$("#rx").val("Bioflu 500g");
		$("#sig").val("Take this medicines for 3 days");
		$("#disp").val("9 pieces");
		$("#refill").val("1 refill");
	}
	if (number == 2) {
		$("#student").val("rafael");
		$("#rx").val("Aspririn 250 mg");
		$("#sig").val("Drink this every 6 hours");
		$("#disp").val("5 pieces");
		$("#refill").val("2 refill");
	}
	if (number == 3) {
		$("#student").val("cedrick");
		$("#rx").val("Medicol 500g");
		$("#sig").val("Take 1 tablet by mouth every six hours for headaches");
		$("#disp").val("5 tablets");
		$("#refill").val("3 refill");
	}
	if (number == 4) {
		$("#student").val("erika");
		$("#rx").val("Livostin");
		$("#sig").val(
			"one drop of Livostin four times per day in each eye for 7 days."
		);
		$("#disp").val("24 tablets");
		$("#refill").val("1 refill");
	}
}
