$(function () {
	// formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});
$(document).ready(function () {
	$("#data-table").DataTable({
		// dom: "Blfrtip",
		dom:
			"<'row'<'col-sm-6'B>>" +
			"<'row'<'col-sm-6'l><'col-sm-6'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-6'i><'col-sm-6'p>>",
		buttons: ["pdf", "excel", "print"],
	});
});
// function to approve consultation
function approveConsultation() {
	Swal.fire({
		title: "Are you sure you want to approve this appointment",
		text: "You won't be able to revert this!",
		icon: "success",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#8f8483",
		confirmButtonText: "Yes, approve it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully Approved a Consultation Appointment");
			var link =
				"mailto:me@example.com" +
				"?cc=myCCaddress@example.com" +
				"&subject=" +
				encodeURIComponent("This is my subject") +
				"&body=" +
				encodeURIComponent(document.getElementById("myText").value);
			window.location.href = link;
		}
	});
}

// function to reject consultation
function rejectConsultation() {
	Swal.fire({
		title: "Are you sure you want to reject this appointment",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, reject it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully Rejected a Consultation Appointment");
		}
	});
}
