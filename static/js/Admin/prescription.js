$(function () {
	formReset("hide");
	// initialize form validation
	$("#form_id").parsley();
});

$(document).ready(function () {
	$("#data-table").DataTable();
});

// function to delete patient
function deletePrescription() {
	Swal.fire({
		title: "Are you sure you want to delete this record",
		text: "You won't be able to revert this!",
		icon: "warning",
		showCancelButton: !0,
		confirmButtonColor: "#34c38f",
		cancelButtonColor: "#f46a6a",
		confirmButtonText: "Yes, delete it!",
	}).then(function (t) {
		if (t.value) {
			toastr.success("Successfully deleted a prescription");
		}
	});
}
