$(function () {
	$("#login-form").on("submit", function (e) {
		e.preventDefault();

		if ($("#login-form").parsley().validate()) {
			$.ajax({
				url: apiURL + "login",
				type: "POST", //method
				data: {
					email: $("#email").val(),
					password: $("#password").val(),
					user_type: $("#user_type").val(),
				},
				dataType: "json",
				success: function (data) {
					console.log("success");

					localStorage.setItem("TOKEN", data.token);

					let session = "";

					session += "token=" + data.token;
					session += "&id=" + data.data.id;
					session += "&first_name=" + data.data.first_name;
					session += "&middle_name=" + data.data.middle_name;
					session += "&last_name=" + data.data.last_name;
					session += "&full_name=" + data.data.full_name;
					session += "&email=" + data.data.email;
					session += "&user_type=" + data.data.user_type;

					window.location.replace(baseURL + "Access/checkSession?" + session);
				},
				error: function ({ responseJSON }) {
					console.log(responseJSON);
					// notification("error", responseJSON.message.join());
					// toastr.error(responseJSON.message.join());
				},
			});
		}
	});
});
